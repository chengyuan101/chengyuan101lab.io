# 机器之心

编程是一种特殊的语言。普通的语言强调指代，讲明思想，不是指令，比如：

```
快跑！
```

这句话说明危险可能来临，大脑立马忘掉其他专注于此，毛细血管收缩，血液流向四肢，瞳孔缩小等等。**但是，你并不一定会跑。** 因为，语言只是指代，并不是指令。让你快跑的人可能正在奋不顾身去解救其他人，你也想参与其中；或者让你快跑的人是你的挚爱，而他被劫匪绑架了。这句语言会让你不单是只想逃跑(flee)，而有可能会是战斗(fight)！所以人和人之间的语言是思想本身，对话是传播观点的方式，不是强加于另一个人的指令，因此语言往往不会参照接受者的逻辑呈现，而是参照自己的思考逻辑来说。

但是编程语言是用于和计算机进行交流，你不得不用机器的逻辑来思考问题。

### 什么是人的逻辑

人的逻辑是主观的，所有人「认为是客观」的事务都是主观的。

比如，甜或咸、亮或暗、生存和死亡是客观。但是，甜和咸都是味蕾上的感受器，接受分子信号传递给大脑的一种感觉；亮或暗都是视网膜上的视杆细胞接受光的物理信号，传递给大脑的视觉中心产生的一种感觉；生存和死亡更是一种主观的认识，生物体内无论生存还是死亡都在进行物理、化学反应过程，只是大脑感受不到他在运动，或者他不能传递信息，或者其他什么的了。

主观，是客观世界的一种坍缩，你永远不能纵览所有，但是从来没有对错。

### 什么是机器逻辑

机器不存在脑补，机器需要明确到底层的指令。什么是正确，什么是错误，什么是字符，什么是数字，什么应该传递，什么应该拒绝，数字怎么处理，字符怎么储存等等。

你说的它一定会做，你说错了它一定会出错。

### 机器的底层语言

说白了，机器最底层的逻辑，也就只有那几样。

- 变量赋值。赋值是机器工作的基础，他让机器能批量解决问题。赋值是封装，不仅可以封装数据，还可以封装方法，甚至可以把方法和资料一起封装起来形成脚手架。
- 数据类型。数据类型让批量解决问题更高效，它亦是一种封装，把相同的数据类型封装在一起，不同的数据用不用的名字来命名。
- 数据计算。对数据进行处理，计算机的核心环节。
- 数据传输。一段代码的能力始终有限，如何把这段代码交给另一段代码，而且中间不会出错，不会被人劫持。


### 机器之心

机器逻辑是机器之心，计算机科学的学生在校并不会学很多语言，更多的是学习机器逻辑（数学、统计学、软件工程、互联网工程、机器学习等）。怎么用几句话来讲明机器之心？

1. 让机器能够已更小的成本，代替人的工作。
2. 让机器能够处于好的架构，从而不断改进，提升效率。
3. 让万物互联，让信息代表意志。
4. 揭示复杂现象背后的科学真理。

未来，单纯的用计算机解决效率问题会通过高度黑箱化的工具实现，用计算机科学揭示复杂现象背后的科学真理，才是前进之路，所以你还要问为什么要学习编程么？
